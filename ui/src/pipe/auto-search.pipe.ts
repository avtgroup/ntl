import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'autoSearch'
})
export class AutoSearchPipe implements PipeTransform {

  transform(value: any, args?: any[]): any {
    if (!args) {
      return value;
    }
    return value.filter((val) => {
      let rVal:string;
      if (val.UserName !== undefined) {
        rVal = (val.UserName.toLocaleLowerCase().includes(args));
      } else if (val.VehicleLicense !== undefined) {
        rVal = (val.VehicleLicense.toLocaleLowerCase().includes(args));
      }
      return rVal;
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Config } from 'src/constant/config';
import { Transaction } from 'src/model/Transaction';
import * as UIkit from 'uikit';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public transactionList: Transaction[] = [];
  public receivingName: string;
  public username: string;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.receivingName ="";
    this.username = "";
    console.log(sessionStorage.getItem("username"))
    if (sessionStorage.getItem("username") !== null && sessionStorage.getItem("username") !== undefined) {
      this.username = sessionStorage.getItem("username");
    }

  }

  searchTransaction(searchValue: string, searchingDate: string, searchingType: string) {
    let param = new HttpParams()
    .set("searchCriteria", searchValue)
    .set("searchType", searchingType)
    .set("searchDate", searchingDate);
    let url = `${Config.Url}/transactions/searchTransaction`;
    this.getTransaction(url, param).subscribe(
      (res: any) => {
        this.transactionList = res;
      }
    )
  }

  private getTransaction(url: string, param: HttpParams) {
    return this.http.get(url, { params: param });
  }

  public CompleteTransaction(TransId: string) {
    let completeObj = {
      "transId" : TransId,
      "receivingName": this.receivingName,
      "pic": sessionStorage.getItem("username")
    }
    this.http.post(`${Config.Url}/transactions/complete`, completeObj).subscribe((res: any) => {
     window.location.reload(); 
    }, 
    error => {
      UIkit.notification({message: 'Có lỗi trong quá cập nhật.', pos: 'bottom-right', status:'danger'})
    })
  }
}

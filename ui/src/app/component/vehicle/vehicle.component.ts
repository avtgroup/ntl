import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from 'src/constant/config';
import { Vehicle } from 'src/model/Vehicle';
import * as UIKit from 'uikit';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss']
})
export class VehicleComponent implements OnInit {
  public vehicleList: Vehicle[] = [];
  public cacheVehicle: Vehicle;
  search:string;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.GetVehicles();
    UIKit.modal('#register-vehicle').hide();
    UIKit.modal('#delete-vehicle').hide();
  }

  public GetVehicles() {
    this.http.get(`${Config.Url}/vehicles`).subscribe((res: any) => {
      res.forEach(element => {
        let vehicle = new Vehicle();
        vehicle.VID = element.VID;
        vehicle.VehicleLicense = element.VehicleLicense;
        this.vehicleList.push(vehicle);
      });
    })
  }

  public registerVehicle(modelId) {
    UIKit.modal('#' + modelId).show();
  }

  public cacheVehicleInfomation(id: string, vehicleLicense: string, modelId: string) {
    this.cacheVehicle = new Vehicle();
    this.cacheVehicle.VID = id;
    this.cacheVehicle.VehicleLicense = vehicleLicense;
    UIKit.modal('#' + modelId).show();
  }

}

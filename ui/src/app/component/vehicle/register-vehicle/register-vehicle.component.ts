import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Config } from 'src/constant/config';

@Component({
  selector: 'app-register-vehicle',
  templateUrl: './register-vehicle.component.html',
  styleUrls: ['./register-vehicle.component.scss']
})
export class RegisterVehicleComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  createVehicleLicense(vehicleLicense: string) {
    this.http.post(`${Config.Url}/vehicles?license=${vehicleLicense}`, {}).subscribe((res: any) => {
     window.location.reload();
    })
  }

}

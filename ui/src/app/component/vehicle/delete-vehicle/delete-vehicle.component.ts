import { Component, OnInit, Input } from '@angular/core';
import { Vehicle } from 'src/model/Vehicle';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Config } from 'src/constant/config';
import * as UIKit from 'uikit';

@Component({
  selector: 'app-delete-vehicle',
  templateUrl: './delete-vehicle.component.html',
  styleUrls: ['./delete-vehicle.component.scss']
})
export class DeleteVehicleComponent implements OnInit {

  @Input() cacheVehicle: Vehicle;

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  deleteVehicle() {
    let param = new HttpParams()
    .set("vehicleId", this.cacheVehicle.VID)
    this.http.delete(`${Config.Url}/vehicles`, {params: param}).subscribe((res: any) => {
      UIKit.modal('#delete-vehicle').hide()
      window.location.reload();
    })
    
  }

}

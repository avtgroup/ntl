import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from 'src/constant/config';
import { Location } from 'src/model/Location';
import * as UIKit from 'uikit';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['./register-employee.component.scss']
})
export class RegisterEmployeeComponent implements OnInit, OnDestroy {

  destroy = new Subject<any>();
  locationList: Location[] =[];
  constructor(private http: HttpClient, private router:Router) { }

  ngOnInit() {
    this.getLocations();
  }

  ngOnDestroy() {
    this.destroy.next()
  }

  getLocations() {
    this.http.get(`${Config.Url}/locations`).subscribe(
      (res: any) => {
        res.forEach(item => {
          var location = new Location();
          location.LID = item.LID;
          location.LocationName = item.LocationName;
          this.locationList.push(location);
        });
      }
    )
  }

  createAccount(username, password, location) {
    if (username === '' || password === '' || location ==='') {
      
    } else {
      let params = {
      "userName": username,
      "password": password,
      "location": location
    };
      this.http.post(`${Config.Url}/employees/create`, params).subscribe(
        (res:any) => {
          this.closeRegisterEmployee('register-employee');
          this.router.navigateByUrl("/employee")
        }
      )
    }
  }

  closeRegisterEmployee(modalId: string) {
    UIKit.modal('#' + modalId).hide();
  }

}

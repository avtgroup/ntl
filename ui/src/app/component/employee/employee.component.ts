import { Component, OnInit } from '@angular/core';
import { Config } from 'src/constant/config';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Employee } from 'src/model/Employee';
import { Location } from 'src/model/Location';
import * as UIKit from 'uikit';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  isFound: Boolean;
  employeeList: Employee[] = [];
  locationList: Location[] = [];
  employee: Employee;
  loadComponent: Boolean = false;

  constructor(private http: HttpClient) { }
  search:string;

  ngOnInit() {
    this.getEmployees();
    UIKit.modal('#register-employee').hide();
    UIKit.modal('#update-employee').hide();
    UIKit.modal('#delete-employee').hide();
  }

  getEmployees(){
    this.http.get(`${Config.Url}/employees`).subscribe(
      (res:any) => {
        res.forEach(item => {
          let employee = new Employee();
          employee.UserId = item.UserId;
          employee.UserName = item.UserName;
          employee.Password = item.Password;
          employee.Location = item.Location;
          this.employeeList.push(employee);
        });
        this.isFound = true;
      },
      () => {
        this.isFound = false;
      }
    )
  }

  cacheAccountInfomation(username:string, password: string, location:string, userId:string, elementId: string) {
    this.employee = new Employee();
    this.employee.UserName = username;
    this.employee.Password = password;
    this.employee.Location = location;
    this.employee.UserId = userId;
    UIKit.modal("#" + elementId).show();
  }

  cacheDeleteAccountInformation(username: string, userId:string) {
    this.employee = new Employee();
    this.employee.UserId = userId;
    this.employee.UserName = username;
    this.deleteEmployee('delete-employee');
  }

  registerEmployee(modalId: string){
    UIKit.modal('#' + modalId).show();
  }

  deleteEmployee(modalId: string){
    UIKit.modal('#' + modalId).show();
  }
}

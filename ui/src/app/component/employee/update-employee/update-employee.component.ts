import { Component, OnInit, Input } from '@angular/core';
import { Employee } from 'src/model/Employee';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Config } from 'src/constant/config';
import { Location } from 'src/model/Location';
import * as UIKit from 'uikit';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.scss']
})
export class UpdateEmployeeComponent implements OnInit {

  @Input() cacheEmployee: Employee;
  locationList: Location[] = [];
  constructor(private http: HttpClient, private router: Router) {}

  ngOnInit() {
    this.cacheEmployee = new Employee();
    this.getLocations();
  }

  getLocations() {
    this.http.get(`${Config.Url}/locations`).subscribe(
      (res: any) => {
        res.forEach(item => {
          var location = new Location();
          location.LID = item.LID;
          location.LocationName = item.LocationName;
          this.locationList.push(location);
        });
      }
    )
  }

  updateAccountInformation(username: string, password: string, location:string) {
    let updatingUsername: string = username !== this.cacheEmployee.UserName ? username : "";
    let updatingPassword: string = password !== this.cacheEmployee.Password ? password : "";
    let updatingLocation: string = location !== this.cacheEmployee.Location ? location : "";

    let params = {
    'username': this.encodeBase64String(updatingUsername),
    'password': this.encodeBase64String(updatingPassword),
    'location': this.encodeBase64String(updatingLocation)
  }
    this.http.post(`${Config.Url}/employees/updateInformation/${this.cacheEmployee.UserId}`, params).subscribe(
      (res:any) => {
        this.closeUpdateEmployee('update-employee');
        window.location.reload();
      },
      error => {
      }
    )
  }

  encodeBase64String(target: string) {
    return btoa(target);
  }

  closeUpdateEmployee(modalId: string) {
    UIKit.modal('#' + modalId).hide();
  }

}

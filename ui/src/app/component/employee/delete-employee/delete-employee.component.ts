import { Component, OnInit, Input } from '@angular/core';
import { Employee } from 'src/model/Employee';
import { HttpClient } from '@angular/common/http';
import { Config } from 'src/constant/config';
import * as UIKit from 'uikit';

@Component({
  selector: 'app-delete-employee',
  templateUrl: './delete-employee.component.html',
  styleUrls: ['./delete-employee.component.scss']
})
export class DeleteEmployeeComponent implements OnInit {

  @Input() cacheDeleteEmployee: Employee;

  constructor(private http: HttpClient) { }

  ngOnInit() {
   this.cacheDeleteEmployee = new Employee();
  }

  public deleteEmployee() {
    this.http.post(`${Config.Url}/employees/delete/${this.cacheDeleteEmployee.UserId}`, {}).subscribe(()=> {
      UIKit.modal('#delete-employee').hide();
      window.location.reload();
    })
    
  }

}

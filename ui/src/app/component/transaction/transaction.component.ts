import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Transaction } from 'src/model/Transaction';
import { HttpClient } from '@angular/common/http';
import { Config } from '../../../constant/config';
import { Router } from '@angular/router';
import * as UIkit from 'uikit'

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

  public transactionList: Array<Transaction> = [];
  public initDate: string = new Date().toLocaleDateString("en-GB");
  active = 0;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    $("table#transaction_tbl input").first().focus();
    let $emptyRow = $('tr:first', $('table'));

    $('table#transaction_tbl').on('keydown', 'input, textarea',  (e)=> {
      var keyCode = e.keyCode;
      if (keyCode === 9) {  
        var $this = $(this),
          $lastTr = $('tr:last', $('table')),
          $lastTd = $('td:last', $lastTr);
        if (($(e.target).closest('td')).is($lastTd)) {
          $lastTr.after(`<tr>${$emptyRow.html()}</tr>`);
        }
      }
    });
  }

  initTransaction() {
    let transaction = new Transaction();
    this.transactionList.push(transaction)
  }

  addTransaction() {
    this.transactionList = [];
    $('#transaction_tbl tr').each((i, e) => {

      let inputArr = $(e).find('td input,td textarea');
     
      if ($(inputArr[0]).val() !== '') {
        let transaction = new Transaction();
        let objKey = Object.keys(transaction);
        objKey.forEach((e, i) => {

          transaction[e] = $(inputArr[i]).val();
          if (e === "SendingLocation") {
            transaction[e] = sessionStorage.getItem("location");
          }
          if (e === "CreatedBy") {
            transaction[e] = sessionStorage.getItem("username");
          }
          if (e === "ResponsibleEmployee") {
            transaction[e] = sessionStorage.getItem("username");
          }  
        })
        this.transactionList.push(transaction);
      }
    })

    let postObj = { "transactionList": this.transactionList };
    var valueArr = postObj.transactionList.map(function (item) { return item.TransId });
    var validatedResult = valueArr.filter(function (item, idx) {
      if (valueArr.indexOf(item) != idx) {
        return item;
      }
    });

    $('#transaction_tbl tr').find('td input, td textarea').removeClass('error');

    if (validatedResult.length > 0) {
      validatedResult.forEach((e, i) => {
        $('#transaction_tbl tr').filter(function (index) {
          return $("td input", this).val() === e;
        })
          .find('td input , td textarea')
          .addClass("error");
      })
    } else {
      this.transactionList.forEach(item => {

      })
      this.http.post(`${Config.Url}/transactions`, postObj)
        .subscribe(() => {
          this.router.navigate(['/transaction'])
          UIkit.notification({message: 'Nhập dữ liệu thành công!', pos: 'bottom-right', status:'primary'})
        },
          error => {
            error.error.forEach((e, i) => {
              $('#transaction_tbl tr').filter(function (index) {
                return $("td input", this).val() !== e;
              })
                .remove();
            })
            UIkit.notification({message: 'Có lỗi trong quá trình nhập', pos: 'bottom-right', status:'danger'})
          })
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { Vehicle } from 'src/model/Vehicle';
import { Config } from 'src/constant/config';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Transaction } from 'src/model/Transaction';
import * as UIkit from 'uikit';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {
  public vehicleLicense: string;
  public vehicleList: Vehicle[] = [];
  public transactionList: Transaction[] = [];
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getVehicle();
  }

  getVehicle() {
    this.http.get(`${Config.Url}/vehicles`).subscribe(
      (res: any) => {
        res.forEach(element => {
          let vehicle = new Vehicle();
          vehicle.VID = element.VID;
          vehicle.VehicleLicense = element.VehicleLicense
          this.vehicleList.push(vehicle);
        });
      }
    )
  }

  getTransactionByVehicle(vehicleLicense: string) {
    let param = new HttpParams()
      .set("vehicleLicense", vehicleLicense)
    this.http.get(`${Config.Url}/transactions/getByVehicle`, { params: param }).subscribe((res: any) => {
      this.transactionList = [];
      res.forEach(element => {
        let transaction = new Transaction();
        transaction = element;
        this.transactionList.push(transaction);
      });
    })
  }

  importTransaction(TransId: string) {
    let transList: string[] = [];
    transList.push(TransId);
    let updateObj = {
      "transactionList": transList,
      "vehicleLicense": "",
      "inChargedPerson": sessionStorage.getItem("username"),
      "location": sessionStorage.getItem("location")
    };
    this.http.post(`${Config.Url}/transactions/update`, updateObj).subscribe(res => {
      UIkit.notification({ message: 'Cập nhật trạng thái thành công!', pos: 'bottom-right', status: 'primary' })
      this.transactionList = this.transactionList.filter(function (obj) {
        return obj.TransId !== TransId;
      });
    },
      error => {
        UIkit.notification({ message: 'Có lỗi trong quá cập nhật.', pos: 'bottom-right', status: 'danger' })
      })
  }
}

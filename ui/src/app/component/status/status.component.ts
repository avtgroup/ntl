import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from 'src/constant/config'
import * as $ from 'jquery';
import * as UIkit from 'uikit';
import { Vehicle } from 'src/model/Vehicle';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {
  vehicleLicense: string;
  vehicleList: Vehicle[] = [];
  transactionList: string[] = [];
  exceptionList: string [] = [];
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getVehicle();
  }

  getVehicle() {
    this.http.get(`${Config.Url}/vehicles`).subscribe(
      (res: any) => {
        res.forEach(element => {
          let vehicle = new Vehicle();
          vehicle.VID = element.VID;
          vehicle.VehicleLicense = element.VehicleLicense
          this.vehicleList.push(vehicle);
        });
      }
    )
  }

  enterTransaction(transaction: string) {
    if (transaction !== '') {
      this.transactionList.push(transaction);
      $('#transaction').val('');
    }
  }

  updateTransactionStatus() {
    let updateObj = {
      "transactionList": this.transactionList,
      "vehicleLicense": this.vehicleLicense,
      "inChargedPerson": sessionStorage.getItem("username"),
      "location": ""
    }
    this.http.post(`${Config.Url}/transactions/update`, updateObj).subscribe(
      (res: any) => {
        this.exceptionList = res;
        if (res.length == 0) {
          UIkit.notification({message: 'Cập nhật trạng thái thành công!', pos: 'bottom-right', status:'primary'})
        } else {
          UIkit.notification({message: 'Có lỗi trong quá cập nhật.', pos: 'bottom-right', status:'danger'})
        }
        this.transactionList = [];
      }
    )
  }

}

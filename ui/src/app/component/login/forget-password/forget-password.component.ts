import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from 'src/constant/config'
import { Router } from '@angular/router';
import * as UIKIt from 'uikit';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  forgetPassword(username: string, newPassword: string, confirmPassword: string) {
    if (newPassword === confirmPassword) {
      let params = {
        "username": username,
        "password": confirmPassword
      };
      this.http.post(`${Config.Url}/employees/forget-password`, params).subscribe(
        (res:any) => {
          this.router.navigateByUrl('/login');
          UIKIt.notification({message: 'Đổi mật khẩu thành công!', pos: 'bottom-right', status:'primary'})
        },
        error => {
          UIKIt.notification({message: 'Đổi mật khẩu thất bại!', pos: 'bottom-right', status:'primary'})
        }
      )
    } else {
      UIKIt.notification({message: 'Xác nhận mật khẩu không trùng khớp!', pos: 'bottom-right', status:'primary'})
    }
  }

}

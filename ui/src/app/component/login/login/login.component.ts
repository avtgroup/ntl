import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from 'src/constant/config';
import { Location } from 'src/model/Location'
import { Router } from '@angular/router';
import * as $ from 'jquery'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  locationList: Location[] = []
  isError = false;
  errorMessage: string;
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    $("#username").first().focus();
    this.getLocations();
  }

  getLocations() {
    this.http.get(`${Config.Url}/locations`).subscribe(
      (res: any) => {
        res.forEach(item => {
          var location = new Location();
          location.LID = item.LID;
          location.LocationName = item.LocationName;
          this.locationList.push(location);
        });
      }
    )
  }

  login(username: string, password: string, location: string) {
    if (username === "" || password === "" || location.includes("---")) {
      this.isError = true;
      this.errorMessage = "Thông tin đăng nhập bị thiếu. Vui lòng kiểm tra lại";
    } else {
      this.http.post(`${Config.Url}/employees/login`, { "username": username, "password": password, "location": location }).subscribe(
        (res: any) => {
          if (res === true) {
            sessionStorage.setItem("username", username);
            sessionStorage.setItem("location", location);
            window.location.reload();
          } else {
            this.isError = true;
            this.errorMessage = "Tài khoản hoặc mật khẩu không đúng. Vui lòng kiểm tra lại";
          }
        }
      )
    }
  }

}

import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { Config } from 'src/constant/config';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Transaction } from '../../../model/Transaction';
import * as $ from 'jquery'
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import * as UIKit from 'uikit';

@Component({
  selector: 'app-view-transaction',
  templateUrl: './view-transaction.component.html',
  styleUrls: ['./view-transaction.component.scss']
})
export class ViewTransactionComponent implements OnInit, OnDestroy, AfterViewInit {
  // Query 
  public from: string = this.FormatDate(true);
  public to: string = this.FormatDate(false);

  // Backend data
  public transData: Transaction[] = [];

  // DataTable
  @ViewChild(DataTableDirective, { static: false })
  public dtElement: DataTableDirective;
  public commentArray: string[] = [];
  public referenceData: Transaction;
  public dtOptions: DataTables.Settings = {};
  public dtTrigger: Subject<any> = new Subject();

  constructor(private http: HttpClient) { }

  ngOnDestroy() {
    this.dtTrigger.unsubscribe();
  }

  ngAfterViewInit() {
    this.dtTrigger.next();
  }

  rerender() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      dtInstance.ajax.reload();
      this.dtTrigger.next();
    })
  }

  ngOnInit() {
    this.referenceData = new Transaction();
    this.drawDataTables();
    UIKit.modal("#updated-comment").hide();
  }

  private drawDataTables(){
    this.dtOptions = {

      responsive: true,
      autoWidth: false,
      ajax: {
        url: `${Config.Url}/transactions/view`,
        dataSrc: "data",
        data: () => {
          return {
            'from': this.from,
            'to': this.to,
          }
        }
      },
      columns: [
        {
          //1
          title: 'Mã VĐ',
          data: 'TransId'
        },
        {
          //2
          title: 'Người gửi',
          data: 'SenderName'
        },
        {
          //3
          title: 'Sdt người gửi',
          data: 'SenderPhone'
        },
        {
          //4
          title: 'Người nhận',
          data: 'ReceiverName'
        },
        {
          //5
          title: 'Sdt người nhận',
          data: 'ReceiverPhone'
        }, {
          //6
          title: 'T.Hàng hóa',
          data: 'MerchandiseType'
        }, {
          //7
          title: 'SL',
          data: 'Quantity'
        },
        {
          //16
          title: 'TT',
          data: 'Status'
        },
        {
          //16
         title: 'Xe',
         data: 'VehicleLicense'
        },
        {
          //18
          title: 'Gửi từ',
          data: 'SendingLocation'
        }, 
        {
          //20
          title: 'Xuống kho',
          data: 'Arrived'
        }
      ],
      rowCallback: (row, data: any) => {
        $(row).click(() => {
          this.showComment(data);
        })
      },
      drawCallback: () => {
        $('select').addClass('form-control form-control-sm');
        $('.dataTables_length').addClass('col-6 float-left');
        $('.dataTables_filter input').addClass('form-control form-control-sm');
      },
      lengthMenu: [5, 10],
      processing: true,
      language: {
        processing: "Đang xử lý...",
        lengthMenu: `<div class="form-group row">
        <label class="col-form-label">Xem </label>
          <div class="col-sm-7">
            _MENU_
          </div>
          <label class="col-form-label">mục</label>
        </div>`,
        zeroRecords: "Không tìm thấy dòng nào phù hợp",
        info: "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
        infoEmpty: "Đang xem 0 đến 0 trong tổng số 0 mục",
        infoFiltered: "(được lọc từ _MAX_ mục)",
        search: "_INPUT_",
        searchPlaceholder: "Tìm kiếm...",
        paginate: {
          first: "Đầu",
          previous: "Trước",
          next: "Tiếp",
          last: "Cuối"
        }
      }
    };
  }

  private FormatDate(isFrom: boolean) {
    let d = new Date();
    let month: string;

    if (isFrom) {
      month = d.getMonth() === 0 ? '' + 11 : '' + d.getMonth();
    }
    else {
      month = '' + (d.getMonth() + 1);
    }

    let day = '' + (d.getDate());
    let year = d.getFullYear();
    return [day, month, year].join('/');
  }

  private showComment(data: any) {
    this.commentArray = [];
    if (data.UpdatedComment !== null) {
      var arr = data.UpdatedComment.split('\n');
      arr.pop();
      this.commentArray = arr.reverse();
    };
    this.referenceData = data;
    // console.log(this.referenceData)

    // Show modal
    UIKit.modal("#updated-comment").show();
  }

  downloadReport() {
    this.http.get(`${Config.Url}/transactions/download?from=${this.from}&to=${this.to}`, {
      responseType: 'arraybuffer'}).subscribe(response => {
      this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    })
  }

  sendReport() {
    this.http.get(`${Config.Url}/transactions/sendReport?from=${this.from}&to=${this.to}&emailTo=trung.nguyen050593@gmail.com&emailCC=`).subscribe(()=> {
      console.log("OK")
    },
    error => {
      console.log(error)
    })
  }

  private downLoadFile(data: any, type: string) {
    let blob = new Blob([data], { type: type});
    let url = window.URL.createObjectURL(blob);
    let pwa = window.open(url);
    if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
        alert( 'Please disable your Pop-up blocker and try again.');
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionCommentComponent } from './transaction-comment.component';

describe('TransactionCommentComponent', () => {
  let component: TransactionCommentComponent;
  let fixture: ComponentFixture<TransactionCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

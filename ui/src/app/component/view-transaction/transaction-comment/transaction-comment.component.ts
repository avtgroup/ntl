import { Component, OnInit, Input } from '@angular/core';
import { Transaction } from 'src/model/Transaction';

@Component({
  selector: 'app-transaction-comment',
  templateUrl: './transaction-comment.component.html',
  styleUrls: ['./transaction-comment.component.scss']
})
export class TransactionCommentComponent implements OnInit {

  @Input() transactionComment: string[];
  @Input() referenceData: Transaction;
  constructor() { }

  ngOnInit() {
    
  }

}

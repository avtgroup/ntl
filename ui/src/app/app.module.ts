import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TransactionComponent } from './component/transaction/transaction.component';
import { SearchComponent } from './component/search/search.component';
import { StatusComponent } from './component/status/status.component';

import { HttpConfigService } from '../services/http-config.service';
import { EmployeeComponent } from './component/employee/employee.component';
import { LoginComponent } from './component/login/login/login.component';
import { ForgetPasswordComponent } from './component/login/forget-password/forget-password.component';
import { RegisterEmployeeComponent } from './component/employee/register-employee/register-employee.component';
import { TransactionLogComponent } from './component/transaction-log/transaction-log.component';
import { UpdateEmployeeComponent } from './component/employee/update-employee/update-employee.component';
import { AutoSearchPipe } from '../pipe/auto-search.pipe';
import { FormatNumberPipe } from '../pipe/format-number.pipe';
import { DeleteEmployeeComponent } from './component/employee/delete-employee/delete-employee.component';
import { ViewTransactionComponent } from './component/view-transaction/view-transaction.component';

import { DataTablesModule } from 'angular-datatables';
import { TransactionCommentComponent } from './component/view-transaction/transaction-comment/transaction-comment.component';
import { VehicleComponent } from './component/vehicle/vehicle.component';
import { RegisterVehicleComponent } from './component/vehicle/register-vehicle/register-vehicle.component';
import { DeleteVehicleComponent } from './component/vehicle/delete-vehicle/delete-vehicle.component';
import { ImportComponent } from './component/status/import/import.component';

@NgModule({
  declarations: [
    AppComponent,
    TransactionComponent,
    SearchComponent,
    StatusComponent,
    EmployeeComponent,
    LoginComponent,
    ForgetPasswordComponent,
    RegisterEmployeeComponent,
    TransactionLogComponent,
    UpdateEmployeeComponent,
    AutoSearchPipe,
    FormatNumberPipe,
    ViewTransactionComponent,
    TransactionCommentComponent,
    DeleteEmployeeComponent,
    VehicleComponent,
    RegisterVehicleComponent,
    DeleteVehicleComponent,
    ImportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    HttpClientModule,
    DataTablesModule
  ],
  providers: [
    HttpConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransactionComponent } from './component/transaction/transaction.component';
import { SearchComponent } from './component/search/search.component';
import { StatusComponent } from './component/status/status.component';
import { EmployeeComponent } from './component/employee/employee.component';
import { LoginComponent } from './component/login/login/login.component';
import { ForgetPasswordComponent } from './component/login/forget-password/forget-password.component';
import { ViewTransactionComponent } from './component/view-transaction/view-transaction.component';
import { VehicleComponent } from './component/vehicle/vehicle.component';
import { ImportComponent } from './component/status/import/import.component';


const routes: Routes = [
  {path: '', redirectTo: '/search', pathMatch: 'full'},
  {path: 'transaction', component: TransactionComponent},
  {path: 'vehicle', component: VehicleComponent},
  {path: 'transactions', component: ViewTransactionComponent},
  {path: 'search', component: SearchComponent},
  {path: 'export', component: StatusComponent},
  {path: 'import', component: ImportComponent, runGuardsAndResolvers: 'always'},
  {path: 'employee', component: EmployeeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'forget-password', component: ForgetPasswordComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }

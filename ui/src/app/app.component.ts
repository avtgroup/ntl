import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ntl';
  usernameSession:string;
  locationSession:string;
  signInTitle: string;
  isShown = false;

  constructor(private router: Router){}
  ngOnInit() {
    this.usernameSession = sessionStorage.getItem("username");
    this.locationSession = sessionStorage.getItem("location");
    
    if (this.usernameSession === null || this.locationSession === null) {
      this.isShown = false;
      this.signInTitle = "Đăng nhập"
    } else {
      this.isShown = true
      this.signInTitle = "Đăng xuất"
      this.router.navigate(['/search'])
    }
  }

  signOut() {
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("location");
    this.isShown = false;
    this.signInTitle = "Đăng nhập"
    this.router.navigate(['/search'])
  }

}

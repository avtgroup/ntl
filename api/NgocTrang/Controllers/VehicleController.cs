﻿using NgocTrangDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NgocTrang.Controllers
{
    [RoutePrefix("vehicles")]
    [EnableCors(origins: "*", headers:"*", methods:"*")]
    public class VehicleController : ApiController
    {
        // GET         
        [Route("")]
        public IHttpActionResult GetVehicles(string search="",bool includeDelete = false)
        {
            try
            {
                // Init DAO
                var vehicleDAO = new VehicleDAO();
                var vehicles = new List<Vehicle>();

                // Switch between search or get all based on search key
                if (string.IsNullOrEmpty(search))
                {
                     vehicles = vehicleDAO.GetVehicles(includeDelete);
                }
                else
                {
                    vehicles = vehicleDAO.FindVehicleByLicenseNumber(search, includeDelete);
                }
                
                // Handle not return any record as an error
                if (vehicles is null && vehicles.Count() == 0)
                {
                   return NotFound();
                }

                return Ok(vehicles);
            }
            catch(Exception)
            {
                return InternalServerError();
            }
        }

        // GET
        [Route("{vehicleId:guid}")]
        public IHttpActionResult GetById(Guid vehicleId)
        {
            try
            {
                // Handle null argument
                if(Guid.Empty==vehicleId)
                {
                    return BadRequest("Vehicle id is invalid");
                }

                // Init DAO
                var vehicleDAO = new VehicleDAO();

                // Get vehicles by id
                var vehicle = vehicleDAO.GetVehicleById(vehicleId);

                // Handle null
                if (vehicle is null)
                {
                    return NotFound();
                }

                return Ok(vehicle);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }


        // POST 
        [Route("")]
        [HttpPost]
        public IHttpActionResult Add(string license)
        {
            try
            {
                if(license is null)
                {
                    return BadRequest("License must have value");
                }

                // Init DAO
                var vehicleDAO = new VehicleDAO();

                // Get vehicles by id
                vehicleDAO.AddVehicle(license);

                return Ok();
            }
            catch(Exception)
            {
                return InternalServerError();
            }
        }

        // PUT 
        [Route("{vehicleId:guid}")]
        [HttpPut]
        public IHttpActionResult Put(Guid vehicleId, [FromBody]string license)
        {
            try
            {
                if (Guid.Empty == vehicleId)
                {
                    return BadRequest("Vehicle id must have value");
                }

                if (license is null)
                {
                    return BadRequest("License must have value");
                }

                // Init DAO
                var vehicleDAO = new VehicleDAO();

                // Get vehicles by id
                vehicleDAO.UpdateVehicle(vehicleId, license);

                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // PUT 
        [Route("{vehicleId:guid}/active")]
        [HttpPut]
        public IHttpActionResult Active(Guid vehicleId)
        {
            try
            {
                if (Guid.Empty == vehicleId)
                {
                    return BadRequest("Vehicle id must have value");
                }

                // Init DAO
                var vehicleDAO = new VehicleDAO();

                // Get vehicles by id
                vehicleDAO.ActiveVehicle(vehicleId);

                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // DELETE 
        [Route("")]
        [HttpDelete]
        public IHttpActionResult Delete(string vehicleId)
        {
            var guid = new Guid(vehicleId);
            try
            {
                if (Guid.Empty == guid)
                {
                    return BadRequest("Vehicle id must have value");
                }

                // Init DAO
                var vehicleDAO = new VehicleDAO();

                // Get vehicles by id
                vehicleDAO.DeleteVehicle(guid);

                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
    }
}

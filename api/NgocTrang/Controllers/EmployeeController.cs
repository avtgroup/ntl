﻿using NgocTrangDAL;
using System;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NgocTrang.Controllers
{
    [RoutePrefix("employees")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EmployeeController : ApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult GetEmployee()
        {
            try
            {
                var employeeDAO = new EmployeeDAO();
                var employeeList = employeeDAO.GetEmployees(false);
                return Ok(employeeList);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [Route("login")]
        [HttpPost]
        public IHttpActionResult Login([FromBody] Login login)
        {
            var employeeDAO = new EmployeeDAO();
            var loginSuccessful = employeeDAO.Login(login.userName, login.password, login.location);
            if (loginSuccessful)
            {
                return Ok(loginSuccessful);
            }
            else
            {
                return Ok(loginSuccessful);
            }
           
        }

        [Route("forget-password")]
        [HttpPost]
        public IHttpActionResult ForgetPassword([FromBody] Account account)
        {
            var employeeDAO = new EmployeeDAO();
            try
            {
                employeeDAO.ForgetPassword(account.username, account.password);
                return Ok();
            }
            catch (DbUpdateException)
            {
                return NotFound();
            }
            catch (Exception)
            {

                return InternalServerError();
            }
        }

        [Route("create")]
        [HttpPost]
        public IHttpActionResult CreateAccount([FromBody] Account account)
        {
            var employeeDAO = new EmployeeDAO();
            try
            {
                employeeDAO.AddUser(account.username, account.password, account.location);
                return Ok();
            }
            catch (DbUpdateException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [Route("updateInformation/{userId}")]
        [HttpPost]
        public IHttpActionResult UpdateAccountInformation(string userId, [FromBody] Account account)

        {
            try
            {
                byte[] userdata = Convert.FromBase64String(account.username);
                var usernameDecoded = System.Text.ASCIIEncoding.ASCII.GetString(userdata);
                userdata = Convert.FromBase64String(account.password);
                var passwordDecoded = System.Text.ASCIIEncoding.ASCII.GetString(userdata);
                userdata = Convert.FromBase64String(account.location);
                var locationDecoded = System.Text.ASCIIEncoding.ASCII.GetString(userdata);
                var employeeDAO = new EmployeeDAO();
                employeeDAO.UpdateInformation(userId, usernameDecoded, passwordDecoded, locationDecoded);
                return Ok();
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.NotFound, "User Not Found");
            }
        }

        [Route("delete/{userId}")]
        [HttpPost]
        public IHttpActionResult DeleteAccount(string userId)

        {
            try
            {
                var employeeDAO = new EmployeeDAO();
                employeeDAO.InactiveAccount(userId);
                return Ok();
            }
            catch (Exception)
            {
                return Content(HttpStatusCode.NotFound, "User Not Found");
            }
        }
    }

    public class Login
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string location { get; set; }

    }

    public class Account
    {
        public string username { get; set; }
        public string password { get; set; }
        public string location { get; set; }
    }


}

﻿using NgocTrangDAL.DAO;
using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NgocTrang.Controllers
{
    [RoutePrefix("locations")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LocationController : ApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult GetLocations()
        {
            try
            {
                var locationsDAO = new LocationDAO();
                return Ok(locationsDAO.GetLocations());
            }
            catch (Exception)
            {

                return InternalServerError();
            }
        }
    }
}

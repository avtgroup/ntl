﻿using NgocTrangDAL;
using NgocTrangDAL.Export;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NgocTrang.Controllers
{
    [RoutePrefix("transactions")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TransactionController : ApiController
    {
        [Route("")]
        [HttpPost]
        public IHttpActionResult CreateTransaction([FromBody]PostObj postObj)
        {

            var exceptionTransId = new List<string>();
            foreach (var item in postObj.transactionList)
            {
                try
                {
                    var transactionDao = new TransactionDAO();
                    transactionDao.AddTransaction(item);
                }
                catch (DbUpdateException)
                {
                    exceptionTransId.Add(item.TransId);
                }
                catch (Exception)
                {
                    return InternalServerError();
                }
            }

            if (exceptionTransId.Count == 0)
            {
                return Ok();
            }
            else
            {
                return Content(HttpStatusCode.NotAcceptable, exceptionTransId);
            }
        }

        [Route("searchTransaction")]
        [HttpGet]
        public IHttpActionResult SearchTransaction(string searchCriteria, string searchType, string searchDate = "")
        {
            var transactionDAO = new TransactionDAO();
            var result = new List<DailyTransaction>();
            try
            {
                switch (searchType)
                {
                    case "Mã vận đơn":
                        result = transactionDAO.GetTransactionById(searchCriteria, searchDate);
                        break;
                    case "Số điện thoại":
                        result = transactionDAO.GetTransactionByPhone(searchCriteria, searchDate);
                        break;
                } 
               
                return Ok(result);
            }
            catch (DbUpdateException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [Route("update")]
        [HttpPost]
        public IHttpActionResult UpdateTransactionStatus([FromBody] UpdateObj updateObj)
        {
            var transactionDAO = new TransactionDAO();
            var exceptionList = new List<string>();

            foreach (var item in updateObj.transactionList)
            {
                try
                {
                    transactionDAO.UpdateTransactionStatus(item, updateObj.inChargedPerson, updateObj.location, updateObj.vehicleLicense);
                }
                catch (Exception)
                {

                    exceptionList.Add(item);
                }
            }

            return Content(HttpStatusCode.OK, exceptionList);
        }

        [Route("view")]
        [HttpGet]
        public IHttpActionResult ViewTransaction(string from,string to,bool includeDelete=false)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from, "d/M/yyyy",System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var transactionDAO = new TransactionDAO();
                var result = transactionDAO.GetTransactionsBetweenDate(fromDate, toDate, includeDelete);
                return Ok(new { data=result });
            }
            catch (DbUpdateException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [Route("sendReport")]
        [HttpGet]
        public IHttpActionResult SendReport(string from, string to, string emailTo, string emailCC = "")
        {
            try
            {
                var fromDate = DateTime.ParseExact(from, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                var transactionDAO = new TransactionDAO();
                var data = transactionDAO.GetTransactionsBetweenDate(fromDate, toDate);
                var dataSet = data.ToDataSet();
                var fileName = $"Báo cáo từ ngày {from} đến ngày {to}.xls";
                MemoryStream fs1 = new MemoryStream();

                var mailMessage = new MailMessage
                {
                    From = new MailAddress("phantienanh1000@gmail.com", "Phan Tien Anh")
                };
                mailMessage.To.Add(emailTo);
                if (!string.IsNullOrEmpty(emailCC))
                {
                    mailMessage.CC.Add(emailCC);

                }
                mailMessage.Body = $"Báo cáo vận đơn từ ngày {from} tới ngày {to}";
                mailMessage.Subject = $"Báo cáo vận đơn từ ngày {from} tới ngày {to}";

                // attach file
                UtilExcel.CreateExcelDocument(dataSet, fs1);
                var ms = new MemoryStream(fs1.ToArray());
                mailMessage.Attachments.Add(new Attachment(ms,fileName));
                
                var client = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential("phantienanh1000@gmail.com", "loveni123"),
                    EnableSsl = true
                };

                client.Send(mailMessage);
                return Ok();
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("download")]
        [HttpGet]
        public HttpResponseMessage DownloadFile(string from, string to)
        {
            try {
                var fromDate = DateTime.ParseExact(from, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                var transactionDAO = new TransactionDAO();
                var data = transactionDAO.GetTransactionsExportBetweenDate(fromDate, toDate);

                if (data != null && data.Count > 0)
                {
                    var dataSet = data.ToDataSet();
                    var fileName = $"DailyTransaction from {from} to {to}.xls";
                    MemoryStream fs1 = new MemoryStream();

                    if (UtilExcel.CreateExcelDocument(dataSet, fs1))
                    {
                        var result = new HttpResponseMessage(HttpStatusCode.OK)
                        {
                            Content = new ByteArrayContent(fs1.ToArray())
                        };
                        result.Content.Headers.ContentDisposition =
                            new ContentDispositionHeaderValue("attachment")
                            {
                                FileName = fileName
                            };
                        result.Content.Headers.ContentType =
                            new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                        return result;
                    }
                    return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                }
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [Route("getByVehicle")]
        [HttpGet]
        public IHttpActionResult GetTransactionByVehicle(string vehicleLicense)
        {
            var transactionDAO = new TransactionDAO();
            var transactionList = new List<DailyTransaction>();
        
                try
                {
                    transactionList = transactionDAO.GetTransactionByVehicle(vehicleLicense);
                }
                catch (Exception)
                {

                return InternalServerError();
                }

            return Content(HttpStatusCode.OK, transactionList);
        }

        [Route("complete")]
        [HttpPost]
        public IHttpActionResult CompleteTransaction([FromBody] CompleteObj completeObj)
        {
            var transactionDAO = new TransactionDAO();

            try
            {
                transactionDAO.CompleteTransaction(completeObj.TransId, completeObj.ReceivingName, completeObj.Pic);
            }
            catch (Exception)
            {

                return InternalServerError();
            }

            return Ok();
        }


        public class PostObj
        {
            public List<DailyTransaction> transactionList { get; set; }
        }

        public class CompleteObj
        {
            public string TransId { get; set; }
            public string ReceivingName { get; set; }
            public string Pic { get; set; }
        }

        public class UpdateObj
        {
            public List<string> transactionList { get; set; }
            public string vehicleLicense { get; set; }
            public string inChargedPerson { get; set; }
            public string location { get; set; }
        }
    }
}

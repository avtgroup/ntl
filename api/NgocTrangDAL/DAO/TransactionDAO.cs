﻿using NgocTrangDAL.Export;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace NgocTrangDAL
{
    public class TransactionDAO
    {
        public void AddTransaction(DailyTransaction transaction)
        {
            try
            {
                using (var dbContext = new ntl16183_NgocTrangEntities())
                {
                    var dailyTransaction = new DailyTransaction
                    {
                        Address = transaction.Address,
                        CN = transaction.CN,
                        Arrived = transaction.Arrived,
                        CreatedDate = DateTime.Now,
                        CreatedBy = transaction.CreatedBy,
                        CT = transaction.CT,
                        Description = transaction.Description,
                        DT = transaction.DT,
                        MerchandiseType = transaction.MerchandiseType,
                        Quantity = transaction.Quantity,
                        ReceiverAddress = transaction.ReceiverAddress,
                        ReceiverName = transaction.ReceiverName,
                        ReceiverPhone = transaction.ReceiverPhone,
                        ReceivingName = transaction.ReceivingName,
                        ReceivingTime = transaction.ReceivingTime,
                        ResponsibleEmployee = transaction.ResponsibleEmployee,
                        SenderName = transaction.SenderName,
                        SenderPhone = transaction.SenderPhone,
                        SendingDay = DateTime.Today,
                        SendingLocation = transaction.SendingLocation,
                        TransId = transaction.TransId,
                        Status = "Đã Tiếp Nhận Vận Đơn",
                        UpdatedBy = transaction.ResponsibleEmployee,
                        UpdatedDate = DateTime.Now,
                        UpdatedComment = $"Vận đơn {transaction.TransId} đã được tiếp nhận bởi {transaction.ResponsibleEmployee} vào lúc {DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss")}" +
                        $"{Environment.NewLine}"

                };

                    dbContext.DailyTransactions.Add(dailyTransaction);
                    dbContext.SaveChanges();
                }
            }
            catch (DbUpdateException)
            {
                throw new DbUpdateException("Duplicated Transaction");
            }
            catch (Exception e)
            {
                throw e;
            }
           
            
        }
        private readonly ntl16183_NgocTrangEntities dbContext = new ntl16183_NgocTrangEntities();

        /// <summary>
        /// Get all daily transactions
        /// </summary>
        /// <param name="includeDelete">Inlcude deleted transactions or not</param>
        /// <returns>List of all daily transactions</returns>
        public List<DailyTransaction> GetTransactions(bool includeDelete)
        {
            try
            {
                if (includeDelete)
                {
                    return dbContext.DailyTransactions.ToList();
                }

                return dbContext.DailyTransactions.Where(p => !p.DeletedDate.HasValue).ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get daily transactions searching by date
        /// </summary>
        /// <param name="searchDate">Date to search daily transaction</param>
        /// <param name="includeDelete">Inlcude deleted transactions or not</param>
        /// <returns>List of transactions searching by date</returns>
        public List<DailyTransaction> GetTransactions(DateTime searchDate, bool includeDelete)
        {
            try
            {
                if (includeDelete)
                {
                    return dbContext.DailyTransactions
                        .Where(p => p.CreatedDate.Value == searchDate)
                        .ToList();
                }

                return dbContext.DailyTransactions
                    .Where(p => p.CreatedDate.Value == searchDate &&
                           !p.DeletedDate.HasValue)
                    .ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get daily transactions between date range
        /// </summary>
        /// <param name="from">Start of date range</param>
        /// <param name="to">End of the date range</param>
        /// <param name="includeDelete">Include deleted transactions or not</param>
        /// <returns></returns>
        public List<DailyTransaction> GetTransactionsIn(DateTime from, DateTime to, bool includeDelete)
        {
            try
            {
                to = to.AddDays(1).AddTicks(-1);

                if (includeDelete)
                {
                    return dbContext.DailyTransactions
                        .Where(p => p.CreatedDate.Value >= from &&
                                    p.CreateDate.Value <= to)
                        .ToList();
                }

                return dbContext.DailyTransactions
                    .Where(p => p.CreatedDate.Value >= from &&
                                p.CreateDate.Value <= to &&
                                !p.DeletedDate.HasValue)
                    .ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get daily transactions between date range by phone number of receiver or sender
        /// </summary>
        /// <param name="from">Start of date range</param>
        /// <param name="to">End of date range</param>
        /// <param name="includeDelete">Include deleted daily transactions or not</param>
        /// <returns></returns>
        public List<DailyTransaction> GetTransactionsBetweenDate(DateTime from, DateTime to, bool includeDelete = false)
        {
            try
            {
                to = to.AddDays(1).AddTicks(-1);

                if (includeDelete)
                {
                    return dbContext.DailyTransactions
                        .Where(p => p.CreatedDate.Value >= from &&
                                    p.CreatedDate.Value <= to)
                        .ToList();
                }

                return dbContext.DailyTransactions
                        .Where(p => p.CreatedDate.Value >= from &&
                                    p.CreatedDate.Value <= to &&
                                    !p.DeletedDate.HasValue)
                        .ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TransactionExportModel> GetTransactionsExportBetweenDate(DateTime from, DateTime to, bool includeDelete = false)
        {
            try
            {
                to = to.AddDays(1).AddTicks(-1);

                if (includeDelete)
                {
                    return dbContext.DailyTransactions
                        .Where(p => p.CreatedDate.Value >= from &&
                                    p.CreatedDate.Value <= to)
                        .Select(p => new TransactionExportModel
                                    {
                                        TransId = p.TransId,
                                        SenderName = p.SenderName,
                                        SenderPhone = p.SenderPhone,
                                        ReceiverName = p.ReceiverName,
                                        ReceiverPhone = p.ReceiverPhone,
                                        MerchandiseType = p.MerchandiseType,
                                        Quantity = p.Quantity,
                                        ReceiverAddress = p.ReceiverAddress,
                                        DT = p.DT,
                                        CT = p.CT,
                                        CN = p.CN,
                                        Description = p.Description,
                                        Address = p.Address,
                                        ReceivingName = p.ReceivingName,
                                        ReceivingTime = p.ReceivingTime,
                                        Status = p.Status,
                                        ResponsibleEmployee = p.ResponsibleEmployee,
                                        SendingDay = p.SendingDay,
                                        SendingLocation = p.SendingLocation,
                                        VehicleLicense = p.VehicleLicense,
                                        Arrived = p.Arrived
                                    }).ToList();

                }
               return dbContext.DailyTransactions
                        .Where(p => p.CreatedDate.Value >= from &&
                                    p.CreatedDate.Value <= to &&
                                    !p.DeletedDate.HasValue)
                        .Select(p => new TransactionExportModel
                                    {
                                        TransId = p.TransId,
                                        SenderName = p.SenderName,
                                        SenderPhone = p.SenderPhone,
                                        ReceiverName = p.ReceiverName,
                                        ReceiverPhone = p.ReceiverPhone,
                                        MerchandiseType = p.MerchandiseType,
                                        Quantity = p.Quantity,
                                        ReceiverAddress = p.ReceiverAddress,
                                        DT = p.DT,
                                        CT = p.CT,
                                        CN = p.CN,
                                        Description = p.Description,
                                        Address = p.Address,
                                        ReceivingName = p.ReceivingName,
                                        ReceivingTime = p.ReceivingTime,
                                        Status = p.Status,
                                        ResponsibleEmployee = p.ResponsibleEmployee,
                                        SendingDay = p.SendingDay,
                                        SendingLocation = p.SendingLocation,
                                        VehicleLicense = p.VehicleLicense,
                                        Arrived = p.Arrived
                                    }).ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get daily transactions between date range by transaction id
        /// </summary>
        /// <param name="from">Start of date range</param>
        /// <param name="to">End of date range</param>
        /// <param name="transId">Id of transaction</param>
        /// <param name="includeDelete">Include deleted daily transactions or not</param>
        /// <returns></returns>
        public List<DailyTransaction> GetTransactionsBetweenDate(DateTime from, DateTime to, Guid transId, bool includeDelete = false)
        {
            try
            {
                to = to.AddDays(1).AddTicks(-1);

                if (includeDelete)
                {
                    return dbContext.DailyTransactions
                        .Where(p => p.CreatedDate.Value >= from &&
                                    p.CreateDate.Value <= to &&
                                    p.TransId.Equals(transId))
                        .ToList();
                }

                return dbContext.DailyTransactions
                        .Where(p => p.CreatedDate.Value >= from &&
                                    p.CreateDate.Value <= to &&
                                    !p.DeletedDate.HasValue &&
                                    p.TransId.Equals(transId))
                        .ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DailyTransaction> GetTransactionById(string searchCriteria, string searchDate)
        {
            var transactionList = new List<DailyTransaction>();
            try
            {
                if (string.IsNullOrEmpty(searchDate))
                {
                    transactionList = dbContext.DailyTransactions.Where(p => p.TransId.Equals(searchCriteria)).ToList();
                }
                else
                {
                   var formatSearchDate = DateTime.ParseExact(searchDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                   transactionList = dbContext.DailyTransactions
                        .Where(p => 
                        p.TransId.Equals(searchCriteria) &&
                        p.SendingDay.Equals(formatSearchDate))
                        .ToList();
                }
               
                return transactionList;
            }
            catch (DbException dbEx)
            {

                throw dbEx;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<DailyTransaction> GetTransactionByPhone(string searchCriteria, string searchDate)
        {
            var transactionList = new List<DailyTransaction>();
            try
            {
                if (string.IsNullOrEmpty(searchDate))
                {
                    transactionList = dbContext.DailyTransactions.Where(p => p.SenderPhone.Equals(searchCriteria)).ToList();
                }
                else
                {

                    var formatSearchDate = DateTime.ParseExact(searchDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    transactionList = dbContext.DailyTransactions.Where(p => p.SenderPhone.Equals(searchCriteria) && p.SendingDay.Equals(formatSearchDate)).ToList();
                }

                return transactionList;
            }
            catch (DbException dbEx)
            {

                throw dbEx;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void UpdateTransactionStatus(string transId, string inChargedPerson, string location="", string vehicleLicense = "")
        {
            var exceptionList = new List<string>();
            try
            {
                var transaction = dbContext.DailyTransactions
                    .Where(p => p.TransId.Equals(transId,StringComparison.OrdinalIgnoreCase))
                    .OrderByDescending(p => p.CreatedDate)
                    .FirstOrDefault();
                   
                if (!string.IsNullOrEmpty(vehicleLicense))
                {
                    // Export Updating
                    transaction.VehicleLicense = vehicleLicense;
                    transaction.UpdatedBy = inChargedPerson;
                    transaction.ResponsibleEmployee = inChargedPerson;

                    transaction.UpdatedDate = DateTime.Now;
                    transaction.Status = "Xuất Kho";
                    transaction.UpdatedComment = transaction.UpdatedComment +
                        $"Vận đơn đã xuất kho {location} lên xe {vehicleLicense}. " +
                        $"Trạng thái hàng được cập nhật bởi {inChargedPerson} vào lúc {transaction.UpdatedDate}" +
                        $"{Environment.NewLine}";
                } else
                {
                    // Import Updating
                    transaction.Arrived = location;
                    transaction.UpdatedBy = inChargedPerson;
                    transaction.ResponsibleEmployee = inChargedPerson;

                    transaction.UpdatedDate = DateTime.Now;
                    transaction.Status = "Nhập Kho";
                    transaction.UpdatedComment = transaction.UpdatedComment +
                        $"Vận đơn đã xuống kho {location} từ xe {transaction.VehicleLicense}. " +
                        $"Trạng thái hàng được cập nhật bởi {inChargedPerson} vào lúc {transaction.UpdatedDate}" +
                        $"{Environment.NewLine}";
                }
              
                dbContext.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<DailyTransaction> GetTransactionByVehicle(string vehicleLicense)
        {
            try
            {
                return dbContext.DailyTransactions
                  .Where(p => p.VehicleLicense.Equals(vehicleLicense) && string.IsNullOrEmpty(p.Arrived))
                  .OrderByDescending(p => p.CreatedDate)
                  .ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void CompleteTransaction(string TransId, string receivingName, string pic)
        {
            try
            {
                var transaction =  dbContext.DailyTransactions
                  .Where(p => p.TransId.Equals(TransId))
                  .OrderByDescending(p => p.CreatedDate)
                  .FirstOrDefault();

                transaction.Status = "Hoàn Thành";
                transaction.ReceivingName = receivingName;
                transaction.ReceivingTime = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                transaction.ResponsibleEmployee = pic;
                transaction.UpdatedBy = pic;
                transaction.UpdatedDate = DateTime.Now;
                transaction.UpdatedComment = transaction.UpdatedComment +
                        $"Vận đơn đã gửi thành công cho {receivingName} vào {transaction.ReceivingTime}. " +
                        $"Trạng thái hàng được hoàn thành bởi {pic} vào lúc {transaction.UpdatedDate}" +
                        $"{Environment.NewLine}";
                dbContext.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}

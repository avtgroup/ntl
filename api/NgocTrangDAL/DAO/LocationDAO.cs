﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;

namespace NgocTrangDAL.DAO
{
    public class LocationDAO
    {
        private readonly ntl16183_NgocTrangEntities dbContext = new ntl16183_NgocTrangEntities();
        public List<Location> GetLocations()
        {
            try
            {
                return dbContext.Locations.ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

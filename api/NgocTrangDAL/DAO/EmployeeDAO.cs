﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace NgocTrangDAL
{
    public class EmployeeDAO
    {
        private readonly ntl16183_NgocTrangEntities dbContext = new ntl16183_NgocTrangEntities();

        /// <summary>
        /// Get all employees
        /// </summary>
        /// <param name="includeDelete">Include deleted employees or not</param>
        /// <returns>List of employees</returns>
        public List<Employee> GetEmployees(bool includeDelete)
        {
            try
            {
                if (includeDelete)
                {
                    return dbContext.Employees.ToList();
                }

                return dbContext.Employees.Where(p => p.InactiveDate != "TRUE").ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Find employee with matching name
        /// </summary>
        /// <param name="name">Partial or full employee name</param>
        /// <returns>Employees with matching name</returns>
        public List<Employee> FindEmployeesByName(string name, bool includeDelete)
        {
            try
            {
                if (name is null)
                {
                    throw new ArgumentNullException();
                }

                if (includeDelete)
                {
                    return dbContext.Employees.Where(p => p.UserName.Contains(name)).ToList();
                }

                return dbContext.Employees.Where(p => p
                    .UserName.Contains(name) &&
                    p.InactiveDate != "TRUE")
                    .ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get employee by user Id
        /// </summary>
        /// <param name="userId">Id of user</param>
        /// <returns>User with matching id</returns>
        public Employee GetEmployeeById(Guid userId)
        {
            try
            {
                if (Guid.Empty == userId)
                {
                    throw new ArgumentException();
                }

                return dbContext.Employees.Find(userId);
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Add new account for employee 
        /// </summary>
        /// <param name="username">Username of employee account</param>
        /// <param name="password">Password of employee password</param>
        public void AddUser(string username,string password, string location)
        {
            try
            {
                if (username is null)
                {
                    throw new ArgumentNullException();
                }
                if (password is null)
                {
                    throw new ArgumentNullException();
                }

                using (var dbContext = new ntl16183_NgocTrangEntities())
                {
                    var newAccount = new Employee
                    {
                        UserId = Guid.NewGuid(),
                        UserName = username,
                        Password = password,
                        Location = location,
                        InactiveDate = "FALSE",
                        CreatedDate = DateTime.Now,
                    };

                    // Check before add new
                    var isExist = dbContext.Employees.Any(p => p
                             .UserName.Equals(username, StringComparison.OrdinalIgnoreCase));

                    if (!isExist)
                    {
                        // Add new
                        dbContext.Employees.Add(newAccount);
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        throw new DbUpdateException();
                    }
                }
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update existed employee account's password
        /// </summary>
        /// <param name="userId">Id of updating account</param>
        /// <param name="newPassword">New password</param>
        /// <param name="currentPassword">Current password</param>
        public void ForgetPassword(string username, string newPassword)
        {
            
            try
            {
                var employee = dbContext.Employees.Where(p => p.UserName.Equals(username)).First();
                employee.Password = newPassword;
                dbContext.SaveChanges();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update employee account's information
        /// </summary>
        /// <param name="userId">Id of employee's account</param>
        /// <param name="information">Information to be update</param>
        public void UpdateInformation(string userId, string username, string password, string location)
        {
            try
            {
                var uID = Guid.Parse(userId);
                var employee = dbContext.Employees.Where(p => p.UserId == uID).First();
                employee.UserName = !string.IsNullOrEmpty(username) && !employee.UserName.Equals(username) ? username : employee.UserName;
                employee.Password = !string.IsNullOrEmpty(password) && !employee.Password.Equals(password) ? password : employee.Password;
                employee.Location = !string.IsNullOrEmpty(location) && !employee.Location.Equals(location) ? location : employee.Location;
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Inactive employee account
        /// </summary>
        /// <param name="userId">Id of employee's account</param>
        public void InactiveAccount(string userId)
        {
            try
            {
                var uID = Guid.Parse(userId);
                var employee = dbContext.Employees.Where(p => p.UserId == uID).First();
                employee.InactiveDate = "TRUE";
                dbContext.SaveChanges();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Reactive employee account
        /// </summary>
        /// <param name="userId">Id of employee's account</param>
        public void ReactiveAccount(Guid userId)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Login(string username, string password, string location)
        {
            try
            {
                if (username.Equals("admin"))
                {
                    return dbContext.Employees.Any(p => 
                    p.UserName.Equals(username) && 
                    p.Password.Equals(password));
                }
                else
                {
                    return dbContext.Employees.Any(p => 
                    p.UserName.Equals(username) && 
                    p.Password.Equals(password) && 
                    p.Location.Equals(location));
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

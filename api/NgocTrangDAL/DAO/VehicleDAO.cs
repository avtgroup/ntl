﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace NgocTrangDAL
{
    public class VehicleDAO
    {
        private readonly ntl16183_NgocTrangEntities dbContext = new ntl16183_NgocTrangEntities();

        /// <summary>
        /// Get all vehicles
        /// </summary>
        /// <param name="includeDelete">Inlcude deleted vehicles or not</param>
        /// <returns>List of vehicle</returns>
        public List<Vehicle> GetVehicles(bool includeDelete)
        {
            try
            {
                if (includeDelete)
                {
                    return dbContext.Vehicles.ToList();
                }
                
                return dbContext.Vehicles.Where(p=>!p.DeletedDate.HasValue).ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Find vehicles with matching license number
        /// </summary>
        /// <param name="licenseNumber">Partial or full license number</param>
        /// <param name="includeDelete">Include deleted vehicles or not</param>
        /// <returns>Vehicles with matching license number</returns>
        public List<Vehicle> FindVehicleByLicenseNumber(string licenseNumber,bool includeDelete)
        {
            try
            {
                if (licenseNumber is null)
                {
                    throw new ArgumentNullException();
                }

                if (includeDelete)
                {
                    return dbContext.Vehicles.Where(p => p.VehicleLicense.Contains(licenseNumber)).ToList();
                }

                return dbContext.Vehicles.Where(p => p
                    .VehicleLicense.Contains(licenseNumber) && 
                    !p.DeletedDate.HasValue)
                    .ToList();
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get vehicle by vehicle id
        /// </summary>
        /// <param name="vehicleId">Id of vehicle</param>
        /// <returns>Vehicle with matching id</returns>
        public Vehicle GetVehicleById(Guid vehicleId)
        {
            try
            {
                if(Guid.Empty== vehicleId)
                {
                    throw new ArgumentException();
                }

                return dbContext.Vehicles.Find(vehicleId);
        }
            catch(DbException dbEx)
            {
                throw dbEx;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get vehicle by license number
        /// </summary>
        /// <param name="licenseNumber">License number of vehicle</param>
        /// <returns>Vehicle with matching license number</returns>
        public Vehicle GetVehicleByLicenseNumber(string licenseNumber)
        {
            if (licenseNumber is null)
            {
                throw new ArgumentNullException();
            }
           
            return dbContext.Vehicles.Where(p=>p
                .VehicleLicense.Equals(licenseNumber,StringComparison.OrdinalIgnoreCase))
                .FirstOrDefault();
        }

        /// <summary>
        /// Add new vehicle 
        /// </summary>
        /// <param name="licenseNumber">License number of new vehicle</param>
        public void AddVehicle(string licenseNumber)
        {
            try
            {
                if (licenseNumber is null)
                {
                    throw new ArgumentNullException();
                }

                using(var dbContext = new ntl16183_NgocTrangEntities())
                {
                    var newVehicle = new Vehicle
                    {
                        VID = Guid.NewGuid(),
                        VehicleLicense = licenseNumber,
                        CreatedDate = DateTime.Now,
                        UpdatedDate= DateTime.Now,
                    };

                    // Check before add new
                    var isExist =dbContext.Vehicles.Any(p => p
                            .VehicleLicense.Equals(licenseNumber, StringComparison.OrdinalIgnoreCase));

                    if (!isExist)
                    {
                        // Add new
                        dbContext.Vehicles.Add(newVehicle);
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        var existedVehicle = dbContext.Vehicles.Where(p => p
                            .VehicleLicense.Equals(licenseNumber, StringComparison.OrdinalIgnoreCase)).First();
                        existedVehicle.DeletedDate = null;
                        dbContext.SaveChanges();
                    }
                }
            }
            catch(DbException dbEx)
            {
                throw dbEx;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update existed vehicle 
        /// </summary>
        /// <param name="vehicleId">Id of updating vehicle</param>
        /// <param name="licenseNumber">New license number to update</param>
        public void UpdateVehicle(Guid vehicleId,string licenseNumber)
        {
            try
            {
                if (Guid.Empty== vehicleId)
                {
                    throw new ArgumentException();
                }

                if(licenseNumber is null)
                {
                    throw new ArgumentException();
                }

                using (var dbContext = new ntl16183_NgocTrangEntities())
                {
                    // Get before update
                    var vehicle = dbContext.Vehicles.Find(vehicleId);

                    if (vehicle!=null)
                    {
                        vehicle.VehicleLicense = licenseNumber;
                        vehicle.UpdatedDate = DateTime.Now;
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        throw new DbUpdateException();
                    }
                }
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Active deleted vehicle 
        /// </summary>
        /// <param name="vehicleId">Id of updating vehicle</param>
        public void ActiveVehicle(Guid vehicleId)
        {
            try
            {
                if (Guid.Empty == vehicleId)
                {
                    throw new ArgumentException();
                }

                using (var dbContext = new ntl16183_NgocTrangEntities())
                {
                    // Get before update
                    var vehicle = dbContext.Vehicles.Find(vehicleId);

                    if (vehicle != null)
                    {
                        if (vehicle.DeletedDate.HasValue)
                        {
                            vehicle.UpdatedDate = DateTime.Now;
                            vehicle.DeletedDate = null;
                            dbContext.SaveChanges();
                        }
                    }
                    else
                    {
                        throw new DbUpdateException();
                    }
                }
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete vehicle with matching id
        /// </summary>
        /// <param name="vehicleId">Id of deleting vehicle</param>
        public void DeleteVehicle(Guid vehicleId)
        {
            try
            {
                if (Guid.Empty==vehicleId)
                {
                    throw new ArgumentException();
                }

                using (var dbContext = new ntl16183_NgocTrangEntities())
                {
                    // Get before update
                    var vehicle = dbContext.Vehicles.Find(vehicleId);

                    if (vehicle != null)
                    {
                        vehicle.DeletedDate = DateTime.Now;
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        throw new DbUpdateException();
                    }
                }
            }
            catch (DbException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿namespace NgocTrangDAL.Export
{
    public class TransactionExportModel
    {
        public string TransId { get; set; }
        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public string MerchandiseType { get; set; }
        public string Quantity { get; set; }
        public string ReceiverAddress { get; set; }
        public string DT { get; set; }
        public string CT { get; set; }
        public string CN { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string ReceivingName { get; set; }
        public string ReceivingTime { get; set; }
        public string Status { get; set; }
        public string ResponsibleEmployee { get; set; }
        public System.DateTime SendingDay { get; set; }
        public string SendingLocation { get; set; }
        public string VehicleLicense { get; set; }
        public string Arrived { get; set; }

    }
}
